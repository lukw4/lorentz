#include "plotWidget.h"
#include <time.h>



PlotWidget::PlotWidget(QWidget* parent, Ui::LorentzClass _ui):QCustomPlot(parent)
{
	qApp->installEventFilter(this);
	ui = _ui;
	this->xAxis->setRange(0, 200);
	this->yAxis->setRange(0, 200);
	skosne1x.append(0);
	skosne1x.append(200);
	skosne1y.append(200);
	skosne1y.append(0);
	skosne2y.append(0);
	skosne2y.append(200);
	xPos = 500;

	double a,b;
	int t, n;
	path[0][0]=path[0][1]=0;
	for(t=0;t<=PL;++t) 
		path[t][1]=0;
	srand (time(NULL));
	//for(n=1;n<10;++n) 
	//{
	//	a=rand()%20000/10000.-1; 
	//	a/=n*n*n*n/20.; 
	//	b=rand()%20000*(M_PI/10000);
	//	for(t=0;t<=PL;++t) 
	//	{
	//	  path[t][1]+=a*sin((2*M_PI/PL)*n*t+b);
	//	}
	//}
	////for(t=PL;t>=0;--t) 
	////	path[t][1]-=path[0][1];
	////path[0][0]=0;
	//for(t=1;t<=PL;++t) 
	//{
	//	a=path[t][1]-path[t-1][1];
	//	path[t][0]=path[t-1][0]+sqrt(1+a*a);
	//}
	change = false;
	currentCount = 0;

}

PlotWidget::~ PlotWidget() {}

void PlotWidget::updateWykres()
{
	QLinkedList<double> tempX;
	QLinkedList<double> tempY;
	QLinkedList<double> tempT;


	double c = 299792458;

	xList.clear();
	yList.clear();
	tList.clear();

	if(currentCount<=100)
	{
		path[currentCount][0] = (xPos)/5.0;
		path[currentCount][1] = 0;

		double a, b;
		a=-xPos/5;
		//a/=1000.0/20.0; 
		//b=xPos * M_PI / 5;
		//path[currentCount][1]+=a*sin((2*M_PI/PL)*10+b);
		path[currentCount][1] = a;

		if(currentCount>0)
		{
			a=path[currentCount][1]-path[currentCount-1][1];
			path[currentCount][0]=path[currentCount-1][0]+sqrt(1+a*a);
		}

		currentCount++;
	}
	else
	{
		currentCount = 101;
		for(int i=0; i<currentCount; i++)
		{
			path[i][0] = path[i+1][0];
			path[i][1] = path[i+1][1];
		}
		currentCount = 100;
		path[currentCount][0] = (xPos)/5.0;
		path[currentCount][1] = 0;

		double a, b;
		a=-xPos/5;
		//a/=1000.0/20.0; 
		//b=xPos * M_PI / 5;
		//path[currentCount][1]+=a*sin((2*M_PI/PL)*10+b);
		path[currentCount][1] = a;
		if(currentCount>0)
		{
			a=path[currentCount][1]-path[currentCount-1][1];
			path[currentCount][0]=path[currentCount-1][0]+sqrt(1+a*a);
		}
		change = !change;
		currentCount++;
	}

	//if(xList.count() >= 400)
	//{
	//	xList.removeFirst();
	//	yList.removeFirst();
	//	tList.removeFirst();
	//}

	//int maxI = yList.size();
	//for(int i=0; i<maxI; i++)
	//{
	//	tempY.push_back(yList.takeFirst());
	//	tempX.push_back(xList.takeFirst());
	//	tempT.push_back(tList.takeFirst());
	//}
	int start = 0;
	//if(!change)
	//	start++;
	for(int i=start; i<currentCount-1; i+=2)
	{

		//for(int n=0;n<100;++n) 
		//{
			//int i=100*n/100;
			int x, y;
			double a, b, da, db, ta, tb;
			a=path[i+1][0]-(da=path[i][0]); 
			b=(db=path[i][1])-path[i+1][1];
			ta=path[currentCount-1][0]; tb=path[currentCount-1][1];
			a/=50.; b/=50.;
			//for(y=0;y<SY;++y)
			//{
			//	//xList.append(y);
			//	//tList.append(y*SX/SY);
			//	//img[y][y*SX/SY]*=.5;
			//}
			//for(y=0;y<SY;++y) 
			//{
			//	//xList.append(y);
			//	//tList.append((SY-y-1)*SX/SY);
			//	//img[y][(SY-y-1)*SX/SY]*=.5;
			//}

			//for(int w=-1;w<=2;++w)
			int w = 0;
			int t = i;
				//for(int t=0;t<PL;t+=50) 
					//dobigspotd(a*(path[t][0]-da-w*ta)+b*(path[t][1]-db-w*tb), b*(path[t][0]-da-w*ta)+a*(path[t][1]-db-w*tb));
				t = 50;
				dobigspotd(a*(path[t][0]-da-w*ta)+b*(path[t][1]-db-w*tb), b*(path[t][0]-da-w*ta)+a*(path[t][1]-db-w*tb));
			
		//}

		/*yList.push_back(tempY.takeFirst() - 0.05);
		double oldX = tempX.takeFirst();

		double v = c*(xPos+500.0)/500.0;
		double gamma = 1/(double)sqrtf(1-(v*v/(c*c)));

		double tVal = tempT.takeFirst() + 0.05;
		double xVal = gamma * (oldX - v * tVal * 0.0000000001);
		tVal = gamma*(tVal - (v * oldX)/(c*c));

		xList.push_back(xVal);
		tList.push_back(tVal);*/

	}
	//xList.push_back((xPos+500.0)/50.0);
	//yList.push_back(10.0);
	//tList.push_back(0.0);
}

void PlotWidget::update()
{   
	/*compute("x_checkbox", spring.list_x, spring.list_t);
	compute("v_checkbox", spring.list_xt, spring.list_t);
	compute("a_checkbox", spring.list_xtt, spring.list_t);
	compute("f_checkbox", spring.list_f, spring.list_t);
	compute("g_checkbox", spring.list_g, spring.list_t);
	compute("h_checkbox", spring.list_h, spring.list_t);
	compute("w_checkbox", spring.list_w, spring.list_t);*/

	compute("skosne1", skosne1y, skosne1x);
	compute("skosne2", skosne2y, skosne1x);

	updateWykres();

	compute("wykres", xList, tList);


	this->replot();
	this->rescaleAxes();

	this->xAxis->setRange(0, 200);
	this->yAxis->setRange(0, 200);
}

void PlotWidget::compute(QString key, QLinkedList<double>& list, QLinkedList<double>& t) {
	int id = graph_map[key];
	if(id >= 0 && id < this->graphCount()) {
		this->graph(id)->setData(t, list);
	}
}

void PlotWidget::addPlot(QString key, QColor& color) {
	this->addGraph();
	int id = this->graphCount() - 1;
	graph_map[key] = id;
	this->graph(id)->setPen(QPen(color)); 
}

void PlotWidget::removePlot(QString key) {
	int id = graph_map[key];
	this->removeGraph(id);
	graph_map[key] = -1;

	std::map<QString, int>::iterator it;
    for (it = graph_map.begin(); it != graph_map.end(); ++it) {
		if((*it).second > id){
			(*it).second--;
		}
	}
}

void PlotWidget::clear() {
	this->clearGraphs();
	std::map<QString, int>::iterator it;
    for (it = graph_map.begin(); it != graph_map.end(); ++it) {
		(*it).second = -1;
	}
}