#ifndef LORENTZ_H
#define LORENTZ_H

#include <QtWidgets/QMainWindow>
#include "ui_lorentz.h"
#include "plotWidget.h"
#include <qtimer.h>

class Lorentz : public QMainWindow
{
	Q_OBJECT

public:
	Lorentz(QWidget *parent = 0);
	~Lorentz();



private:
	Ui::LorentzClass ui;

	PlotWidget *widget;

	QTimer* timer;

};

#endif // LORENTZ_H
