/********************************************************************************
** Form generated from reading UI file 'lorentz.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LORENTZ_H
#define UI_LORENTZ_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LorentzClass
{
public:
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;

    void setupUi(QMainWindow *LorentzClass)
    {
        if (LorentzClass->objectName().isEmpty())
            LorentzClass->setObjectName(QStringLiteral("LorentzClass"));
        LorentzClass->resize(1000, 1000);
        centralWidget = new QWidget(LorentzClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(-10, 0, 1011, 991));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        LorentzClass->setCentralWidget(centralWidget);

        retranslateUi(LorentzClass);

        QMetaObject::connectSlotsByName(LorentzClass);
    } // setupUi

    void retranslateUi(QMainWindow *LorentzClass)
    {
        LorentzClass->setWindowTitle(QApplication::translate("LorentzClass", "Lorentz", 0));
    } // retranslateUi

};

namespace Ui {
    class LorentzClass: public Ui_LorentzClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LORENTZ_H
