#ifndef PLOTWIDGET_H
#define PLOTWIDGET_H

#include "3rdparty/qcustomplot.h"
#include <QtWidgets>
#include <QGLWidget>
#include "ui_lorentz.h"

#define SX 200
#define SY 200
#define PL 100

class PlotWidget : public QCustomPlot
{ 
  Q_OBJECT
  
public:   
    PlotWidget(QWidget* parent,  Ui::LorentzClass _ui);
    ~PlotWidget();
	void addPlot(QString key, QColor& color);
	void removePlot(QString key);
	void clear();

	QLinkedList<double> skosne1x;
	QLinkedList<double> skosne1y;
	QLinkedList<double> skosne2y;

	
	QLinkedList<double> xList;
	QLinkedList<double> yList;
	QLinkedList<double> tList;

	QTimer* timer;
	double lastTime;

	int currentCount;

	double path[PL+1][2];


	void updateWykres();

	int xPos;

	void mouseMoveEvent(QMouseEvent* event) 
	{
		xPos = -event->pos().y();
		qDebug() << QString::number(event->pos().x());
		qDebug() << QString::number(event->pos().y());
	}

	bool eventFilter(QObject *obj, QEvent *event)
	{
	  if (event->type() == QEvent::MouseMove)
	  {
		QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
		//statusBar()->showMessage(QString("Mouse move (%1,%2)").arg(mouseEvent->pos().x()).arg(mouseEvent->pos().y()));
	  }
	  return false;
	}

	void dodot(int x, int y, double f) 
	{
	  if(x<0||x>=SX||y<0||y>=SY) return;
	  //img[y][x]*=f;
	}

	void dospot(int x, int y) 
	{
	  dodot(x, y, .5);
	  dodot(x+1, y, .75);
	  dodot(x-1, y, .75);
	  dodot(x, y+1, .75);
	  dodot(x, y-1, .75);
	}
 
	void dospotd(double t, double x) 
	{
	  dospot((x+1)*(SX/2.), (-t+1)*(SY/2.));
	}

	void dobigspot(int x, int y) 
	{
	  //int a, b;
	  //for(b=-3;b<=3;++b) 
		//  for(a=-3;a<=3;++a) 
		//	  if(a*a+b*b<=9) 
		//		  dodot(x+a, y+b, (a*a+b*b)/10.);
		xList.append(x);
		tList.append(y);
	}
 
	void dobigspotd(double t, double x) 
	{
	  dobigspot((x+1)*(SX/2.), (-t+1)*(SY/2.));
	}

	bool change;



public slots:
	void update();

public:
	Ui::LorentzClass ui;

private: 
	void compute(QString key, QLinkedList<double>& list, QLinkedList<double>& t);

	std::map<QString, int> graph_map;


};


#endif