#include "lorentz.h"


Lorentz::Lorentz(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	

	widget = new PlotWidget(this, ui);

    QSizePolicy policy1 = widget->sizePolicy();
    policy1.setHorizontalStretch(1);
	policy1.setVerticalStretch(1);
    widget->setSizePolicy(policy1);

	ui.verticalLayout->addWidget(widget);
    setLayout(ui.verticalLayout);

	widget->addPlot("skosne1", QColor(0, 0, 0));
	widget->addPlot("skosne2", QColor(0, 0, 0));
	widget->addPlot("wykres", QColor(255, 0, 0));

	timer = new QTimer(this);
	  
    connect(timer,SIGNAL(timeout()),widget,SLOT(update()));
    timer->start(10);

	widget->timer = timer;

}

Lorentz::~Lorentz()
{

}
